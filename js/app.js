/**
 * Project - Snake Game
 * Description - This project made with HTML5 canvas and javascript
 * Author - Debasis Nath
 */


let gameSpeed;
const CANVAS = document.getElementById("box");
const CTX = CANVAS.getContext("2d");
const BLOCKSIZE = 25;
const EASY = document.getElementById("easy");
const MEDIUM = document.getElementById("medium");
const HARD = document.getElementById("hard");
const RESET = document.getElementById("reset");

let snake = [
  {x: 150, y: 150},
  {x: 140, y: 150},
  {x: 130, y: 150},
  {x: 120, y: 150},
  {x: 110, y: 150}
]

let score = 0
let changingDirection = false;
let foodX;
let foodY;
let dx = BLOCKSIZE;
let dy = 0;

EASY.addEventListener("click", (e) => {
    gameSpeed = 150;
    startGame()
    createFood()
})
MEDIUM.addEventListener("click", (e) => {
    gameSpeed = 100;
    startGame()
    createFood()
})
HARD.addEventListener("click", (e) => {
    gameSpeed = 50;
    startGame()
    createFood()
})
RESET.addEventListener("click", () => {
    document.location.reload(true)
})

document.addEventListener("keydown", changeDirection);


/**
 * Main function to start the game
 */
function startGame() {
  if (endGame()) return gameOver();

  setTimeout(function onTick() {
    changingDirection = false;
    clearCanvas();
    drawFood();
    moveTheSnake();
    drawSnake();

    startGame();
  }, gameSpeed)
}


/**
 * Clears the canvas 
 */
function clearCanvas() {
  CTX.fillStyle = "#000";
  CTX.strokestyle = "red";
  CTX.fillRect(0, 0, CANVAS.width, CANVAS.height);
  CTX.strokeRect(0, 0, CANVAS.width, CANVAS.height);
}

/**
 * @param foodX - x Coordinate, foodY - y Coordinate
 * Draws a rect at random position
 */

function drawFood() {
  CTX.fillStyle = "purple";
  CTX.strokestyle = "green";
  CTX.fillRect(foodX, foodY, BLOCKSIZE, BLOCKSIZE);
  CTX.strokeRect(foodX, foodY, BLOCKSIZE, BLOCKSIZE);
}

/**
 * @param dx - x Coordinate speed, dy - y Coordinate speed
 * Moves the snake position by dx and dy speed 
 */

function moveTheSnake() {
  const head = {x: snake[0].x + dx, y: snake[0].y + dy};
  snake.unshift(head);

  const eatFood = snake[0].x === foodX && snake[0].y === foodY;
  if (eatFood) {
    score += 10;
    document.querySelector('#score p').textContent = `SCORE- ${score} `;
    createFood();
  } else {
    snake.pop();
  }
}

/**
 * Returns True if the snake hits itself OR the wall
 */
function endGame() {
  for (let i = 4; i < snake.length; i++) {
    if (snake[i].x === snake[0].x && snake[i].y === snake[0].y) return true
  }

  const hitLeftWall = snake[0].x < 0;
  const hitRightWall = snake[0].x > CANVAS.width - BLOCKSIZE;
  const hitToptWall = snake[0].y < 0;
  const hitBottomWall = snake[0].y > CANVAS.height - BLOCKSIZE;

  return hitLeftWall || hitRightWall || hitToptWall || hitBottomWall
}


/**
 * @param min - min size, max - max size, BLOCKSIZE - rect size
 * Generates a random position for food coordinates
 */
function randomPostion(min, max) {
  return Math.round((Math.random() * (max-min) + min) / BLOCKSIZE) * BLOCKSIZE;
}

/**
 * create foodX and foodY positions
 */

function createFood() {
  foodX = randomPostion(0, CANVAS.width - BLOCKSIZE);
  foodY = randomPostion(0, CANVAS.height - BLOCKSIZE);

  snake.forEach(function isFoodOnSnake(part) {
    const foodInSnake = part.x == foodX && part.y == foodY;
    if (foodInSnake) createFood();
  });
}

/**
 * Paint rects(creates food rect) at random position in the canvas if the
 * Game is over 
 */
function gameOver(){
    return setInterval(() => {
        createFood()
        snake.forEach(part => {
            drawSnakePart(part,"purple","#000")
        })
        drawFood()
    })
}

/**
 * Paints rects for each postion of snake
 */
function drawSnake() {
  snake.forEach(part => {
      drawSnakePart(part,"red","#fff")
  })
}

/**
 * @param snakePart - Object with x and y coordinates
 * @param innerColor - fill color of rect
 * @param outerColor - stroke color of rect
 * Draws a single rect for coordinates in snakePart object
 */

function drawSnakePart(snakePart,innerColor, outerColor) {

  CTX.fillStyle = innerColor;
  CTX.strokestyle = outerColor;
  CTX.fillRect(snakePart.x, snakePart.y, BLOCKSIZE, BLOCKSIZE);
  CTX.strokeRect(snakePart.x, snakePart.y, BLOCKSIZE, BLOCKSIZE);
}
/**
 * @param event object from keyboard event
 * Sets dx and dy value according to keyCode
 */

function changeDirection(event) {
  const LEFT_KEY = 37;
  const RIGHT_KEY = 39;
  const UP_KEY = 38;
  const DOWN_KEY = 40;

  if (changingDirection) return;

  changingDirection = true;
  
  const keyPressed = event.keyCode;

  const goingUp = dy === -BLOCKSIZE;
  const goingDown = dy === BLOCKSIZE;
  const goingRight = dx === BLOCKSIZE;
  const goingLeft = dx === -BLOCKSIZE;

  if (keyPressed === LEFT_KEY && !goingRight) {
    dx = -BLOCKSIZE;
    dy = 0;
  }
  
  if (keyPressed === UP_KEY && !goingDown) {
    dx = 0;
    dy = -BLOCKSIZE;
  }
  
  if (keyPressed === RIGHT_KEY && !goingLeft) {
    dx = BLOCKSIZE;
    dy = 0;
  }
  
  if (keyPressed === DOWN_KEY && !goingUp) {
    dx = 0;
    dy = BLOCKSIZE;
  }
}